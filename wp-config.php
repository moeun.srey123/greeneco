<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */

// set_time_limit(300);

define( 'DB_NAME', 'greeneco' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '%e0gATyGME*,Ucm92?@(o?<e(i^c>Ub<5fXt;93Hv/|cAO=*+.yAvl[DFkx/UtoU' );
define( 'SECURE_AUTH_KEY',  ';jlrNxv}S;jT5O(oui,5li0~~vbiw6-pIjgg/9=svyYbxXD3W(~[Z%}&fZw23I3!' );
define( 'LOGGED_IN_KEY',    'lsw>8~5?m/$L]|LBE|2Y]|0*}(yCm]`5Szoh!<$N@DoUw)Qm:!2Jol!yy:tVFdVL' );
define( 'NONCE_KEY',        '=la0TMosX6YZLy2t*>>:jcn,Wj;ichno!POg`C1Ud7Q u.U<.RlBo|jooBx;_!~p' );
define( 'AUTH_SALT',        '8qaKTkCU;zmCm rcag!Bj8P58guERvP=Fs}Z3P~<V]C@:1HMHpUW.7AW!/NpHiZl' );
define( 'SECURE_AUTH_SALT', 'k2V=PfL@U$+KW21g0!PS~*AN7&7K;W)5v{8oTF5W%]TYQ;k1@?U3DheKIly?wUP$' );
define( 'LOGGED_IN_SALT',   'yKBIie9W(W3;LyOm1:rLhJIZCl8u:-AK5$E?L6J36WXk|q|fysLX/kMeWzTVf,kM' );
define( 'NONCE_SALT',       'bQ O1pU%S[c0>[5(1m1HojYne G_,6UE.+BB{gBLQE%;d>cGluyxhKL:;,1>Sk{9' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
